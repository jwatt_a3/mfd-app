import React, { Component } from "react";
import socketIOClient from "socket.io-client";
class App extends Component {
  constructor() {
    super();
    this.state = {
      init: false,
      data: false
    };
  }
  componentDidMount() {
    const socket = socketIOClient();
    socket.on("data", data => {
      console.log('new data');
      console.log(data);
      this.setState({ init: true, x: data.position[0], y: data.position[1], z: data.position[2] });
    });
  }
  render() {
    const { init, x, y, z } = this.state;
    console.log(x);
    return (
        <div style={{ textAlign: "center" }}>
          {init
              ? <p>{"x: " + x + ", y: " + y + ", z: " + z}</p>
              : <p>Waiting for Data...</p>}
        </div>
    );
  }
}
export default App;