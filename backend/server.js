const express = require('express');
const path = require('path');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const state = require('./state.js');

app.use(express.static(path.join(__dirname, '..', 'frontend', 'build')));
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '..', 'frontend', 'build', 'index.html'));
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

app.delete('/queue', function (req, res) {
    const queue = state.emptyQueue();
    return res.status(200).json(queue);
});

app.put('/data', bodyParser.json(), function (req, res) {
    state.updateData(req.body);
    io.emit('data', state.getData());
    res.status(200).send('success');
});

http.listen(8080, () => {
  console.log('listening on *:8080');
});