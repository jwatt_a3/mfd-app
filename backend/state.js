const state = {
    queue: [],
    data: {
        position: [0, 0, 0]
    }
};

module.exports = {
    pushMessage: (msg) => {
        state.queue.push(msg)
    },
    emptyQueue: () => {
        const queue = state.queue;
        state.queue = [];
        return queue;
    },
    updateData: (data) => {
        state.data = data;
    },
    getData: () => {
        return state.data;
    }
};
